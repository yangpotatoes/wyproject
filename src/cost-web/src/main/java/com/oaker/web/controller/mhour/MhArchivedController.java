package com.oaker.web.controller.mhour;


import com.oaker.common.core.domain.AjaxResult;
import com.oaker.hours.doman.dto.MhArchiveDTO;
import com.oaker.hours.doman.dto.MhArchiveQueryDTO;
import com.oaker.hours.doman.vo.MhArchiveDetailVO;
import com.oaker.hours.doman.vo.MhArchiveVO;
import com.oaker.hours.doman.entity.MhArchive;

import com.oaker.hours.service.MhArchiveService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@RestController
@Slf4j
@Api(tags = "归档管理")
@RequestMapping("/mh/archive")
public class MhArchivedController {

    @Autowired
    private MhArchiveService mhArchiveService;

    @GetMapping("/list")
    //@PreAuthorize("@ss.hasPermi('mh:archive:list')")
    public AjaxResult queryList(MhArchiveQueryDTO mhArchiveQueryDTO) {
        //List<UserHourListVO> list = userHourService.queryList(startDate, endDate);

        List<MhArchiveVO> list =  mhArchiveService.queryList(mhArchiveQueryDTO);
        return AjaxResult.success(list);
    }


    @PostMapping
    //@PreAuthorize("@ss.hasPermi('mh:archive:create')")
    public  AjaxResult create(@RequestBody MhArchiveDTO mhArchiveDTO) {
        long result = mhArchiveService.create(mhArchiveDTO.getDeptId(),mhArchiveDTO.getArchiveDate());
        return  AjaxResult.success(result);
    }

    @GetMapping("/{archiveId}")
    public AjaxResult query(@PathVariable  Long archiveId) {
        log.info("查询归档详情,{}",archiveId);
        MhArchiveDetailVO mhArchiveDetailVO = mhArchiveService.query(archiveId);
        return  AjaxResult.success(mhArchiveDetailVO);
    }

}
